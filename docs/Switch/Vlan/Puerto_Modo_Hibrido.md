# Configuración modo Híbrido

En este modo, se puede utilizar un puerto como modo acceso (para aquellas vlans que no tengan etiquetas) y  modo troncal (para aquellas que sean etiquetadas):

```bash
<Huawei>system-view
[Huawei]interface "tipo" "#i"
[Huawei-interfaz#i]port link-type hybrid 
[Huawei-interfaz#i]port hybrid untagged vlan "#v"
[Huawei-interfaz#i]port hybrid tagged vlan "#v"
[Huawei-interfaz#i]quit
```

Donde: |||
-- | -- | -- |
**tipo** = tipo de interfaz | **#i** = identificador de la interfaz | **#v** = Número de vlan / all
