# Asignación VLAN

## Escenarios de asignación de vlans

| Método de asignación de VLAN | Ventaja | Desventaja | Escenario de uso |
| -- | -- | -- | -- |
| Basado en puertos | Método más usado y de simple configuración. | No es flexible. Si un puerto necesita enviar tramas a otra vlan, se debe reconfigurar. | Aplicable a redes a gran escala con pocos requerimientos de seguridad.
| Basado en direcciones MAC | Las vlans no deben ser re-asignadas cuando un usuario viaja de un lugar a otro. Este modo provee seguridad y flexibilidad. | El administrador de red debe configurar cada asociación MAC-VLAN. | Aplicable a redes donde los usuarios tienen movilidad. |
| Basado en subredes IP | IP subnet-based and protocol-based VLAN assignment are both network layer-based VLAN assignment. | Network layer-based VLAN assignment greatly reduces workload of manual configurations and allows users to easily join a VLAN, move from one VLAN to another, or leave a VLAN. | The switch needs to parse the source IP addresses of packets and convert them into MAC addresses. This slows down switch response. | Applicable to networks that have traveling users and require simple management. |
| Basado en Protocolos | El switch necesita analizar direciones de cada protocolo. Se obtiene un rendimiento bajo. | Puede asignarse vlans basadas en los protocolos: AppleTalk, IPv4, IPv6, e IPX .  |
