## Visualización de todas las vlans

La siguiente visualización muestra una tabla en donde se listan todas las vlans creadas en el dispositivo con los puertos asociados y el modo en que las interfaces han sido configuradas.

```bash
display vlan
--------------------------------------------------------------------------------
U: Up;         D: Down;         TG: Tagged;         UT: Untagged;
MP: Vlan-mapping;               ST: Vlan-stacking;
#: ProtocolTransparent-vlan;    *: Management-vlan;
--------------------------------------------------------------------------------

VID  Type    Ports                                                          
--------------------------------------------------------------------------------
"#v" common  "etiqueta":"interfaz"("estado_interfaz")                                     

VID  Status     Property      MAC-LRN        Statistics       Description      
--------------------------------------------------------------------------------
"#v" "estado_v" default       "aprendizaje"  "estadística"    "nombre"       
```

Donde: ||
-- | -- 
**#v** = número de vlan | **etiqueta** = TG (con etiqueta) / UT (sin etiqueta)
**interfaz** = interfaz a la que se le asignó la vlan | **estado interfaz** = U (alto) / D (baja)
**aprendizaje** = enable (analiza direcciones MAC) / disable (No analiza direcciones MAC) | **estadística** = disable / enable (almacena datos estadísticos de la vlan)
**nombre** = nombre/dirección asociado a la vlan

## Visualización de estado de una vlan en particular

Al indicar el número de vlan al final del siguiente comando, se obtiene como resultado una tabla como es en el caso anterior, pero sólo de la vlan indicada.

```bash
display vlan "#v"
```

donde: |
-- |
**#v** = número de vlan |
