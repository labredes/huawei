# Configuración modo Troncal

## Asignación a una interfaz

```bash
<Huawei>system-view
[Huawei]interface "tipo" "#i"
[Huawei-interfaz#i]port link-type trunk
[Huawei-interfaz#i]port trunk allow-pass vlan "#v"
```

Donde: | | |
-- | -- | --
**tipo** = tipo de interfaz	| **#i** = identificador de la interfaz | **#v** = Número de vlan / all

## Asignación a un grupo de interfaces

```bash
<Huawei>system-view
[Huawei]port-group "nombre"
[Huawei-port-group-nombre]group-member "tipo" "#i" to "tipo" "#i"
[Huawei-port-group-nombre]port link-type trunk
[Huawei-port-group-nombre]port trunk allow-pass vlan "#v"
[Huawei-port-group-nombre]quit

```

Donde: | | |
-- | -- | --
**nombre** = nombre asignado al grupo | **tipo** = tipo de interfaz |
 **#i** = identificador de la interfaz | **#v** = número de vlan |
