# Direcciones IP

Configuración correspondiente a los modelos: AR100, AR120, AR150, AR160, AR200, AR1200, AR2200, AR3200 y AR3600

## Visualización de direcciones IP configuradas 

El comando **display** permite la visualización del estado y configuración del equipo. Puede ejecutarse desde cualquier parte del árbol de configuración y se muestra en pantalla aquello que se le ingrese como argumento.

### Visualización breve
```bash
<Huawei>display ip interface brief
```

### Visualización completa
```bash
<Huawei>display ip interface 
```

## Asignación de dirección IP a una interfaz 
```bash
<Huawei> system-view
[Huawei]interface Vlanif "#"
[Huawei-Vlanif#]ip address "x.x.x.x" "y o y.y.y.y"
```

Donde: | | |
-- | -- | -- |
**#** = número de vlan | **x.x.x.x** = dirección IP | **y o y.y.y.y** = máscara de red (decimal) | 
