# Opciones de visualización de ERPS

## Visualización del estado de ERPS

```
<Huawei>system-view
[Huawei]display erps ring "#r"
D : Discarding
F : Forwarding
R : RPL Owner

Ring   Control   WRT Timer   Guard Timer   Port 1              Port 2
ID     VLAN      (min)       (csec)
-------------------------------------------------------------------------------
"#r"   "#vc"                               "estado""puerto"    "estado""puerto"
-------------------------------------------------------------------------------
```

Donde: | |
-- | -- |
**#r** = número de anillo | **#vc** = número de vlan de control |
**estado** = D/F/R | **puerto** = puerto asignado a ERPS |
