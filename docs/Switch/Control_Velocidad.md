# Control de velocidad por vlan

Pasos a configurar:

1- Crear vlans y asociarlas a los puertos (ver secciones de configuración de vlans).

2- Configurar el clasificador de tráfico basado en identificadores de vlan.

3- Configurar comportamiento de tráfico en el Switch para limitar la velocidad de los paquetes y modificar su prioridad.

4- Configurar una política de tráfico en el Switch, relacionando el clasificador y el comportamiento de tráfico.

## Creación de clasificador y asignación de vlans

```
<Huawei>system-view
[Huawei]traffic classifier "nom_clas" operator and
[Huawei-classifier-"nom_clas"] if-match vlan-id "#v" 
```

Donde: | |
-- | -- 
**nom_clas** = nombre del clasificador | **#v** = identificador de vlan 

## Configuración comportamiento de tráfico

```
<Huawei>system-view
[Huawei] traffic behavior "nom_beh"
[Huawei-behavior-"nom_beh"] car cir 2000 pir 10000 green pass  //Set the CIR of packets with VLAN 120 to 2000 kbit/s.
[Huawei-behavior-"nom_beh"] remark dscp 46  //Configure the device to re-mark DSCP priorities of packets from VLAN 120 with 46.
[Huawei-behavior-"nom_beh"] statistic enable  //Enable traffic statistics.
```

Donde: | |
-- | -- 
**nom_beh** = nombre de comportamiento | **#v** = identificador de vlan 

## Configuración de política de tráfico

```
<Huawei>system-view
[Huawei] traffic policy "nom_pol"
[Huawei-trafficpolicy-"nom_pol"] classifier nom_clas behavior nom_beh
[Huawei-trafficpolicy-"nom_pol"] quit
[Huawei]interface "tipo" "#i"
[Huawei-interfaz#i] traffic-policy "nom_pol" inbound
[Huawei-interfaz#i] quit
```

Donde: | |
-- | -- 
**nom_pol** = nombre de política | **nom_clas** = nombre del clasificador
**nom_beh** = nombre de comportamiento | **tipo** = tipo de interfaz
**#i** = identificador de la interfaz | 

## Visualización de la configuración

