# Direcciones IP

Configuración correspondiente a los modelos: AR100, AR120, AR150, AR160, AR200, AR1200, AR2200, AR3200 y AR3600

## Visualización de direcciones IP configuradas 

El comando **display** permite la visualización del estado y configuración del equipo. Puede ejecutarse desde cualquier parte del árbol de configuración y se muestra en pantalla aquello que se le ingrese como argumento.

### Visualización breve
```bash
<Huawei>display ip interface brief
```

### Visualización completa
```bash
<Huawei>display ip interface 
```

## Asignación de dirección IP a una interfaz 
```bash
<Huawei> system-view
[Huawei]interface "interfaz" "z/z/z"
[Huawei-interfazz/z/z]ip address "x.x.x.x" "y o y.y.y.y"
```

Donde: | |
-- | -- |
**interfaz** = nombre lógico de la interfaz | **z/z/z** = número de interfaz |
**x.x.x.x** = dirección IP | **y o y.y.y.y** = máscara de red (decimal) | 

## Asignación de dirección IP secundaria a una interfaz 
```bash
<Huawei> system-view
[Huawei]interface "interfaz" "z/z/z"
[Huawei-interfazz/z/z]ip address "x.x.x.x" "y o y.y.y.y" sub
```

Donde: | |
-- | -- |
**interfaz** = nombre lógico de la interfaz | **z/z/z** = número de interfaz |
**x.x.x.x** = dirección IP | **y o y.y.y.y** = máscara de red (decimal) | 

## Eliminar direcciones IP asignadas a una interfaz

Para eliminar una IP asignada a una interfaz, anteponer el argumento **undo** a la configuración:

```bash
<Huawei> system-view
[Huawei]interface "interfaz" "z/z/z"
[Huawei-interfazz/z/z]undo ip address "x.x.x.x" "y o y.y.y.y"
```

Donde: | |
-- | -- |
**interfaz** = nombre lógico de la interfaz | **z/z/z** = número de interfaz |
**x.x.x.x** = dirección IP | **y o y.y.y.y** = máscara de red (decimal) | 
